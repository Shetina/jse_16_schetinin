package ru.t1.schetinin.tm.api.model;

import ru.t1.schetinin.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
